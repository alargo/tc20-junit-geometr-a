package TEAM1.TC20;





import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import dto.Geometria;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testareacuadrado()
    {
        int resultado = Geometria.areacuadrado(5);
        int esperado = 25;
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testareaCirculo()
    {
        double resultado = Geometria.areaCirculo(25.5);
        double esperado = 2042.82;
        int delta = 1;
        assertEquals(esperado,resultado, delta);
    }
    
    @Test
    public void testareatriangulo()
    {
        int resultado = Geometria.areatriangulo(6, 6);
        int esperado = 18;
        int delta = 1;
        assertEquals(esperado,resultado, delta);
    }
    
    @Test
    public void testarearectangulo()
    {
        int resultado = Geometria.arearectangulo(25, 5);
        int esperado = 125;
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testareapentagono()
    {
        int resultado = Geometria.areapentagono(8, 9);
        int esperado = 36;
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testarearombo()
    {
        int resultado = Geometria.arearombo(40, 2);
        int esperado = 40;
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testarearomboide()
    {
        int resultado = Geometria.arearomboide(20, 2);
        int esperado = 40;
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testareatrapecio()
    {
        int resultado = Geometria.areatrapecio(4, 5, 5);
        int esperado = 22;
        int delta = 1;
        assertEquals(esperado,resultado,delta);
    }
    
    @Test
    public void testfigura1()
    {
        String resultado = Geometria.figura(1);
        String esperado = "cuadrado";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura2()
    {
        String resultado = Geometria.figura(2);
        String esperado = "Circulo";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura3()
    {
        String resultado = Geometria.figura(3);
        String esperado = "Triangulo";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura4()
    {
        String resultado = Geometria.figura(4);
        String esperado = "Rectangulo";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura5()
    {
        String resultado = Geometria.figura(5);
        String esperado = "Pentagono";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura6()
    {
        String resultado = Geometria.figura(6);
        String esperado = "Rombo";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura7()
    {
        String resultado = Geometria.figura(7);
        String esperado = "Romboide";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testfigura8()
    {
        String resultado = Geometria.figura(8);
        String esperado = "Trapecio";
        assertEquals(esperado,resultado);
    }
    
    @Test
    public void testgetArea()
    {
        Geometria p1 = new Geometria(); 
        assertEquals(2.5, p1.getArea());
    }
    
    @Test
    public void testgetId()
    {
        Geometria p1 = new Geometria(); 
        assertEquals(9,p1.getId());
    }
    
    @Test
    public void testgetNom()
    {
        Geometria p1 = new Geometria(); 
        assertEquals("Default",p1.getNom());
    }
    
    @Test
    public void testsetId()
    {
        Geometria p1 = new Geometria(); 
        p1.setId(15);
        int resultado = p1.getId();
        int esperado = 15;
        assertEquals(resultado,esperado);
    }
    
    @Test
    public void testsetNom()
    {
        Geometria p1 = new Geometria(); 
        p1.setNom("Prueba");
        String resultado = p1.getNom();
        String esperado = "Prueba";
        assertEquals(resultado,esperado);
    }
    
    @Test
    public void testsetArea()
    {
        Geometria p1 = new Geometria(); 
        p1.setArea(2.5);
        double resultado = p1.getArea();
        double esperado = 2.5;
        assertEquals(resultado,esperado);
    }
    
    
   
    
    
    
    
}
